package password;

/*
 * @author Shinnosuke Noda
 * Student Number: 991527227
 * This class validate passwords.
 * 
 */

public class PasswordValidator {

	private static final int MIN_LENGTH = 8;
	
	private static final int MIN_DIGITS = 2;
	
	public static boolean hasEnoughDigits( String password ) {
		/*
		int countDigits = 0;
		
		for ( int i = 0; i < password.length(); i++) {
			char word = password.charAt(i);
			
			// If it is a number, it will increment the count.
			if (Character.isDigit(word)) {
				countDigits++;
			}
		}
		
		if ( countDigits >= MIN_DIGITS ) {
			return true;
		} else {
			return false;
		}
		*/
		
		// Refactoring
		// Used the regular expression to make the code cleaner
		// \\D : non-digits. It replaces all non-digits to empty string.
		return password.replaceAll( "\\D" , "" ).length() >= MIN_DIGITS ? true : false;
	}
	
	/**
	 * Validates length of passwords, blank spaces are not considered valid characters
	 * @param password
	 * @return
	 */
	public static boolean isValidLength( String password ) {
		/*
		int passwordLength = password.length();;
		
		if (passwordLength < MIN_LENGTH) {
			return false;
		} else {
			return true;
		}
		*/
		
		// Refactoring
		if ( password != null ) {
			return password.trim().length() >= MIN_LENGTH;
		} else {
			return false;
		}
	}
		
}
