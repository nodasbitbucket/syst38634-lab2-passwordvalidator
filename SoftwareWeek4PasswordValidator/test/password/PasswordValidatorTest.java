package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	/*
	 * The password should be included at least 2 digits.
	 * This will pass 5 digits, so it is enought.
	 */
	@Test
	public void testHasEnoughDigitsRegular() {
		assertTrue( "Invalid number of digits", PasswordValidator.hasEnoughDigits("12345ABCDE") );
	}
	
	/*
	 * This will pass password which has not digits and its length is 10.
	 */
	@Test
	public void testHasEnoughDigitsExceptional() {
		assertFalse( "Invalid number of digits", PasswordValidator.hasEnoughDigits("ABCDEFGHIJ") );
	}
	
	/*
	 * It passes the minimum required digits which are 2.
	 */
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue( "Invalid number of digits", PasswordValidator.hasEnoughDigits("12ABCDEFGHIJ") );
	}
	
	/*
	 * It will pass only 1 digit, so it will fail.
	 */
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse( "Invalid number of digits", PasswordValidator.hasEnoughDigits("1ABCDEFGHIJ") );
	}
	
	
	
	
	
	
	
	// These methods for passwords' length.
	/*
	 * The password length should be greater than 8.
	 * This test will pass 10 characters, so it should pass.
	 */
	@Test
	public void testIsValidLengthRegular() {
		assertTrue( "Invalid password length", PasswordValidator.isValidLength("1234567890") );
	}
	
	/*
	 * This will pass an empty string. It should fail.
	 */
	@Test
	public void testIsValidLengthException() {
		assertFalse( "Invalid password length", PasswordValidator.isValidLength("") );
	}
	
	/*
	 * The minimum length of valid password is 8.
	 * In this edge case, it will pass 8 characters.
	 */
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue( "Invalid password length", PasswordValidator.isValidLength("12345678") );
	}
	
	/**
	 * This should fail. The string's length is only 7.
	 */
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
}
